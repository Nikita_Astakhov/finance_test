from django.db import models

from django.contrib.postgres.fields import HStoreField

# Create your models here.
class Currency(models.Model):
	name = models.CharField(max_length=15)

	def __str__(self):
		return self.name

class user_wallet(models.Model):
	user_id = models.BigIntegerField(default=-1)
	transaction_id = models.BigIntegerField(default=0)
	currency = models.CharField(max_length=10)
	creation_date = models.DateTimeField(auto_now_add=True)
	amount = models.DecimalField(max_digits=11, decimal_places=2, default=000000000.00)
	balance = HStoreField()


# wallet_id user_id transaction_id currency amount balance(key value)

	