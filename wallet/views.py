from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status


from wallet.models import *
from wallet.serializers import *
from wallet.utility import *
# - Create your views here. - As you wish Lord.

class some_items_set_pagination(PageNumberPagination):
	page_size = 15 


class list_currencys(generics.ListAPIView):
	serializer_class = currencys_serializer

	def get_queryset(self):
		result = Currency.objects.all()
		return result	


#получить все балансы пользователя во всех валютах. Опциональный параметр currency, в котором можно запросить только баланс в одной конкретной валюте.
class get_balance(generics.ListAPIView): 
	serializer_class = user_balance_serializer

	def get_queryset(self):
		user_id = self.kwargs['user_id']
		# and for filtering for balance of specified currency --> !!! Filter on the client side !!!
		result = user_wallet.objects.filter(user_id=user_id).order_by('creation_date').reverse()[:1]
		return result


class get_transactions(generics.ListAPIView):
	pagination_class = some_items_set_pagination
	serializer_class = user_transactions_serializer

	def get_queryset(self):
		user_id = self.kwargs['user_id']
		result = user_wallet.objects.filter(user_id=user_id).order_by('creation_date').reverse()
		return result						

# request body
#{
# "user_id": "1",
# "transaction_id": "400", !!! change it everytime !!!
# "amount": "1000",
# "currency": "USD"
#}

class credit(APIView):
	permission_classes = (AllowAny,)

	def post(self, request, format=None):
		user_id = int(request.data['user_id'])
		transaction_id = int(request.data['transaction_id'])
		amount = request.data['amount']
		currency = str(request.data['currency'])

		response = Utilities.update_operation(user_id, transaction_id, currency, amount, 1)
		return response


class debit(APIView):
	permission_classes = (AllowAny,)

	def post(self, request, format=None):
		user_id = int(request.data['user_id'])
		transaction_id = int(request.data['transaction_id'])
		amount = request.data['amount']
		currency = str(request.data['currency'])

		response = Utilities.update_operation(user_id, transaction_id, currency, amount, 2)
		return response											  