from wallet.models import *
from rest_framework import serializers

class user_balance_serializer(serializers.ModelSerializer):
	class Meta:
		model = user_wallet
		fields = ('user_id', 'balance')

class user_transactions_serializer(serializers.ModelSerializer):
	class Meta:
		model = user_wallet
		fields = '__all__'

class currencys_serializer(serializers.ModelSerializer):
	class Meta:
		model = Currency
		fields = '__all__'			