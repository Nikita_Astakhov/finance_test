from rest_framework.response import Response
from rest_framework import status

from decimal import Decimal

from wallet.models import *

# credit - 1 – зачислить средства на баланс пользователя в указанной валюте.
# debit - 2 - списать средства с баланса пользователя в указанной валюте.
# transaction_id - его передает вызывающее приложение/сервис, 
#например это можно быть идентификатор транзакции в платежной системе, 
#откуда произошло зачисление или другой внутренний идентификатор. 
#Необходимо это с целью предотвращение множественных начислений или списаний при 
#конкурентном вызове определенных методов приложения для одной операции. 
#То есть вызовы, содержащие дублирующий transaction_id необходимо отклонять.
### What will happen?
# 1. Change wallet balance
# 2. Save transaction

class Utilities(object):

	def update_operation(user_id, transaction_id, currency, amount, what):
		response = Response({'text': 'Something went wrong. Please reload the page.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		
		# check if transaction already exists
		try:
			transaction_obj = user_wallet.objects.get(transaction_id=transaction_id, user_id=user_id)
			response = Response({'text': 'Already in a quey.'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		except user_wallet.DoesNotExist:
			# the wallet must exist anyway. it need to be done during registration, 
			# if unknown user exist in the system all unauthorized operations go through his wallet. 
			
			# steps
			# 1. get last one
			# 2. create new with summed values
			# 3. save

			# value of last transaction of the user is better to store and get in/from Redis, but not this time.
			# probably it may be better only for addition because with substraction client can get negative balance
			last_transaction_obj = user_wallet.objects.filter(user_id=user_id).order_by('creation_date').values('balance').reverse()[:1].get()
			
			value = Utilities.check_currency_exist(last_transaction_obj, currency)
			# avaliability of the cuurency in clients account
			if not value:
				response = Response({'text': 'Transaction terminated. The client do not registered such currency in his account.'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
			else:
				response = Utilities.calculate_value(what, value, amount, last_transaction_obj, user_id, transaction_id, currency) 	
		
		return response

	def check_currency_exist(last_transaction_obj, currency):
		# get the value in current currency
		# check what operations could be complited relaying on balance
		try:
			dictbalance = last_transaction_obj['balance']
			value = dictbalance[currency]
		except KeyError:
			value = False
			# the user have no such cuurency 
			# !will not do this here!
			# if credit then crete new 
			# if debit then raise error for transaction or make transaction from another currency
		return value

	def compare_subtraction(value, amount):
		result = [False, 0.0]
		minus = Decimal(value) - Decimal(amount)
		if minus >= Decimal(0):
			result = [True, Decimal(minus)]
		return result			

	def create_transaction(last_transaction_obj, updated_currency_value, user_id, transaction_id, currency, amount):
		response = Response({'text': 'Write error. Please contact support.'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		
		last_transaction_obj['balance'][currency] = updated_currency_value	
		
		transaction_data = {'user_id': user_id, 'transaction_id': transaction_id, 
							'amount': Decimal(amount), 'currency': currency,
							'balance': last_transaction_obj['balance']}
		transaction_obj = user_wallet.objects.create(**transaction_data)
		# possibly then created obj --> Redis or --> Rabbit --> Celery --> Reddis
		response = Response({'text': 'Operation completed.'}, status=status.HTTP_200_OK)
		return response		

	def calculate_value(what, value, amount, last_transaction_obj, user_id, transaction_id, currency):
		# perform addition or subsctraction
		granted = False
		if what == 1:
			updated_currency_value = Decimal(value) + Decimal(amount)
			granted = True
		else:
			compare = Utilities.compare_subtraction(value, amount)
			if compare[0]:
				updated_currency_value = compare[1]
				granted = True
			else:
				response = Response({'text': 'Transaction terminated. The client does not have enough funds on the account.'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

		# save the transaction data	
		if granted:
			response = Utilities.create_transaction(last_transaction_obj, updated_currency_value, user_id, transaction_id, currency, amount)	
		return response