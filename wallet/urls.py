from django.urls import path
from . import views

app_name = 'wallet'

urlpatterns = [
	path('balance/<int:user_id>/', views.get_balance.as_view(), name='get_client_balance'),
	path('transactions/<int:user_id>/', views.get_transactions.as_view(), name='get_client_transactions'),
	path('credit/', views.credit.as_view(), name='credit_client_balance'),
	path('debit/', views.debit.as_view(), name='debit_client_balance'), 

	path('currenthy/', views.list_currencys.as_view(), name='list_all_currenthy'),

]